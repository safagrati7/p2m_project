import os
from django.apps import AppConfig
from keras.models import load_model

class DiseaseAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'disease_app'
    model = None  # Placeholder for the loaded model

    def ready(self):
        # Define the relative path to the model file
        relative_model_path = "final_model.keras"

        # Get the directory of the current Python script
        script_directory = os.path.dirname(os.path.abspath(__file__))

        # Construct the absolute path to the model file
        model_file_path = os.path.abspath(os.path.join(script_directory, relative_model_path))

        # Load your model
        if not self.model:
            try:
                self.model = load_model(model_file_path)
            except FileNotFoundError:
                print("Model file not found. Please check the path:", model_file_path)
